using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Используем команды Unity
using UnityEngine;

// Работаем с инструментами Photon.Pun
using Photon.Pun;

// Используем подключения к серверам Photon
using Photon.Realtime;

// Работаем с коллекциями
using System.Collections.Generic;// Наследуем MainMenuStateChanger от BaseStateChanger
public class MainMenuStateChanger : BaseStateChanger
{
    // Версия игры
    [SerializeField] private string _gameVersion = "1";

// Максимальное число игроков в комнате
    [SerializeField] private int _maxPlayersPerRoom = 2;


    
    public override void OnConnectedToMaster()
    {
        // НОВОЕ: Если мы не в лобби
        if (!PhotonNetwork.InLobby)
        {
            JoinLobby();
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        // Вызываем метод Connect()
        Connect();
    }

    public override void OnJoinedLobby()
    {
        // Отображаем экран лобби
        ScreensController.ShowScreen<LobbyScreen>();
    }
    public override void OnJoinedRoom()
    {
        RoomScreen roomScreen = ScreensController.ShowScreen<RoomScreen>();
        roomScreen.SetRoomNameText(PhotonNetwork.CurrentRoom.Name);

        // НОВОЕ: Задаём список игроков
        roomScreen.PlayerListView.SetPlayers(PhotonNetwork.PlayerList);

        // НОВОЕ: Вызываем метод RefreshPlayButton()
        RefreshPlayButton();
        RefreshRoomVisible();
        GameStateChanger.ReadyCount = 0;
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        // Отображаем экран ошибки
        ErrorScreen errorScreen = ScreensController.ShowScreen<ErrorScreen>();

        // Устанавливаем текст ошибки
        errorScreen.SetErrorText($"Код ошибки: {returnCode}; сообщение: {message}");
    }

    public override void OnLeftRoom()
    {
        ScreensController.ShowPrevScreen();

        // НОВОЕ: Получаем окно комнаты
        RoomScreen roomScreen = ScreensController.GetScreen<RoomScreen>();

        // НОВОЕ: Очищаем его контейнер
        roomScreen.PlayerListView.ClearContainer();
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        // Запрашиваем экран лобби
        LobbyScreen lobbyScreen = ScreensController.GetScreen<LobbyScreen>();

        // Передаём на этот экран список комнат
        lobbyScreen.RoomListView.SetRoomList(roomList);
    }
// Обновляем видимость текущей комнаты в сети
    private void RefreshRoomVisible()
    {
        // Проверяем, что текущий клиент — главный (мастер-клиент)
        if (PhotonNetwork.IsMasterClient)
        {
            // Устанавливаем видимость комнаты в зависимости от количества игроков
            // Если количество игроков в комнате меньше максимально допустимого, делаем комнату видимой
            // В противном случае (если комната полная), она становится невидимой
            PhotonNetwork.CurrentRoom.IsVisible = PhotonNetwork.CurrentRoom.PlayerCount < PhotonNetwork.CurrentRoom.MaxPlayers;
        }
    }
    // НОВОЕ: Переопределили метод из BaseStateChanger
    protected override void OnInit()
    {
        PhotonNetwork.AutomaticallySyncScene = true;

        Connect();

        LobbyScreen lobbyScreen = ScreensController.GetScreen<LobbyScreen>();
        lobbyScreen.RoomListView.OnJoinRoomButtonClick += JoinRoom;

        CreateRoomScreen createRoomScreen = ScreensController.GetScreen<CreateRoomScreen>();
        createRoomScreen.OnCreateRoomButtonClick += CreateRoom;

        RoomScreen roomScreen = ScreensController.GetScreen<RoomScreen>();
        roomScreen.OnLeaveButtonClick += LeaveRoom;
        roomScreen.OnPlayButtonClick += ScenesLoader.LoadGame;
    }
    private void Connect()
    {
        if (PhotonNetwork.IsConnected)
        {
            JoinLobby();
        }
        else
        {
            ScreensController.ShowScreen<LoadingScreen>();

            // НОВОЕ: Убрали переменную _isConnecting
            PhotonNetwork.ConnectUsingSettings();

            PhotonNetwork.GameVersion = _gameVersion;
        }
    }

    private void JoinLobby()
    {
        // Входим в лобби
        PhotonNetwork.JoinLobby();

        // Отображаем экран загрузки
        ScreensController.ShowScreen<LoadingScreen>();
    }
    private void CreateRoom(string name)
    {
        // Просим Photon создать новую комнату
        // С определённым именем и настройками
        // Где указывается максимальное число игроков
        PhotonNetwork.CreateRoom(name, new RoomOptions() { MaxPlayers = _maxPlayersPerRoom });

        // Отображаем экран загрузки
        ScreensController.ShowScreen<LoadingScreen>();
    }

    private void JoinRoom(string name)
    {
        // Просим Photon подключить нас к комнате
        // С определённым именем
        PhotonNetwork.JoinRoom(name);

        // Отображаем экран загрузки
        ScreensController.ShowScreen<LoadingScreen>();
    }

    private void LeaveRoom()
    {
        // Просим Photon отключить нас от комнаты
        PhotonNetwork.LeaveRoom();

        // Не отображаем экран загрузки (присваиваем ему false)
        ScreensController.ShowScreen<LoadingScreen>(false);
    }
    private void OnDestroy()
    {
        // Если контроллера экранов нет
        if (!ScreensController)
        {
            // Выходим из метода
            return;
        }
        // Запрашиваем экран создания комнаты
        CreateRoomScreen createRoomScreen = ScreensController.GetScreen<CreateRoomScreen>();

        // Если есть экран создания комнаты
        if (createRoomScreen)
        {
            // Отписываемся от события OnCreateRoomButtonClick
            createRoomScreen.OnCreateRoomButtonClick -= CreateRoom;
        }
        // Запрашиваем экран лобби
        LobbyScreen lobbyScreen = ScreensController.GetScreen<LobbyScreen>();

        // Если есть экран лобби со списком комнат
        if (lobbyScreen && lobbyScreen.RoomListView)
        {
            // Отписываемся от события OnJoinRoomButtonClick
            lobbyScreen.RoomListView.OnJoinRoomButtonClick -= JoinRoom;
        }
        // Запрашиваем экран комнаты
        RoomScreen roomScreen = ScreensController.GetScreen<RoomScreen>();

        // Если есть экран комнаты
        if (roomScreen)
        {
            roomScreen.OnLeaveButtonClick -= LeaveRoom;

            // НОВОЕ: Отписываемся от события OnPlayButtonClick
            roomScreen.OnPlayButtonClick -= ScenesLoader.LoadGame;
        }
    }
    // Вызывается при входе игрока в комнату
    public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
    {
        // Получаем окно комнаты
        RoomScreen roomScreen = ScreensController.GetScreen<RoomScreen>();

        // Добавляем игрока
        roomScreen.PlayerListView.AddPlayer(newPlayer);

        // Вызываем метод RefreshPlayButton()
        RefreshPlayButton();
        RefreshRoomVisible(); 
    }
// Вызывается при выходе игрока из комнаты
    public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
    {
        // Получаем окно комнаты
        RoomScreen roomScreen = ScreensController.GetScreen<RoomScreen>();

        // Удаляем игрока
        roomScreen.PlayerListView.RemovePlayer(otherPlayer);

        // Вызываем метод RefreshPlayButton()
        RefreshPlayButton();
        RefreshRoomVisible();
    }
// Вызывается при сетевой смене игрока (мастер-клиента)
    public override void OnMasterClientSwitched(Photon.Realtime.Player newMasterClient)
    {
        // Вызываем метод RefreshPlayButton()
        RefreshPlayButton();
        RefreshRoomVisible(); 
    }
// Обновляем кнопку «Начать игру»
    private void RefreshPlayButton()
    {
        // Получаем окно комнаты
        RoomScreen roomScreen = ScreensController.GetScreen<RoomScreen>();

        // Кнопка «Начать игру» активна только на мастер-клиенте
        // И когда комната не пустая
        bool isActive = PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers;
        roomScreen.SetActivePlayButton(isActive);
    }
}
