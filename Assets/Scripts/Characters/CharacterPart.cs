using UnityEngine;// Используем инструменты Photon.Pun
using Photon.Pun;
// НОВОЕ: Сделали наследование от MonoBehaviourPunCallbacks
public abstract class CharacterPart : MonoBehaviourPunCallbacks
{
    // Флаг активности части
    protected bool IsActive;
// Переменная для работы с сетевым представлением объекта
    private PhotonView _photonView;

// Свойство для доступа к _photonView
// Из дочерних классов
    public PhotonView PhotonView
    {
        get
        {
            if (!_photonView)
            {
                _photonView = GetComponent<PhotonView>();
            }
            return _photonView;
        }
    }
    public void Activate()
    {
        // Делаем часть активной
        IsActive = true;
    }
    // Инициализируем переменные
    public void Init()
    {
        // Делаем часть активной
        IsActive = true;

        // Вызываем метод OnInit()
        OnInit();
    }
    // Останавливаем часть персонажа
    public void Stop()
    {
        // Делаем часть неактивной
        IsActive = false;

        // Вызываем метод OnStop()
        OnStop();
    }
    // Защищённый виртуальный метод OnInit()
    protected virtual void OnInit() { }

    // Защищённый виртуальный метод OnStop()
    protected virtual void OnStop() { }
}