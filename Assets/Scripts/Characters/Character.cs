using UnityEngine;

// Сделали класс абстрактным
public abstract class Character : MonoBehaviour
{
    // Массив частей персонажа
    private CharacterPart[] _parts;

    // Вызывается при запуске игры
    private void Start()
    {
        // Вызываем метод Init()
        Init();
    }
    public void Activate()
    {
        // Проходим по всем частям персонажа
        for (int i = 0; i < _parts.Length; i++)
        {
            // Активируем каждую
            _parts[i].Activate();
        }
    }
    // Заполняем ссылки на компоненты
    public void Init()
    {
        // Получаем компоненты частей персонажа
        _parts = GetComponents<CharacterPart>();

        // Проходим по всем частям
        for (int i = 0; i < _parts.Length; i++)
        {
            // Вызываем у каждой метод Init()
            _parts[i].Init();
        }
        // Вызываем метод InitDeath()
        InitDeath();
    }
    // Получаем часть персонажа по типу T
// То есть ограничиваем метод так, что он работает
// Только с объектами, которые наследуются от CharacterPart
    private T GetPart<T>() where T : CharacterPart
    {
        // Проходим по всем частям
        for (int i = 0; i < _parts.Length; i++)
        {
            // Если текущая часть соответствует типу T
            if (_parts[i] is T part)
            {
                // Возвращаем эту часть
                return part;
            }
        }
        // Возвращаем пустое значение
        return null;
    }
    // Инициализируем гибель персонажа
    private void InitDeath()
    {
        // НОВОЕ: Получаем компонент здоровья персонажа 
        var health = GetPart<CharacterHealth>();

        // НОВОЕ: Обрабатываем событие OnDie без цикла
        health.OnDie += Stop;
    }
    // Останавливаем персонажа
    public void Stop()
    {
        // Проходим по всем частям
        for (int i = 0; i < _parts.Length; i++)
        {
            // Вызываем у каждой метод Stop()
            _parts[i].Stop();
        }
    }
}