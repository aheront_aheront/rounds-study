// Сделали класс абстрактным
// Наследуем его от CharacterPart
public abstract class CharacterMovement : CharacterPart
{
    // А здесь ничего не нужно
}