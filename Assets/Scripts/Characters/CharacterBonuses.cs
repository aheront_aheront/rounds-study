﻿using System.Collections.Generic;

// Используем инструменты Photon.Pun
using Photon.Pun;

// Работаем с коллекциями
using System.Collections;
// Сделали класс абстрактным
// Наследуем его от CharacterPart
public abstract class CharacterBonuses : CharacterPart
{
    // Список активированных бонусов
    private List<BonusType> _existingBonusTypes = new List<BonusType>();

    // Массив объектов применения бонусов
    private List<BonusApplier> _bonusAppliers = new List<BonusApplier>()
    {
        new ShootCountBonusApplier(),
    };
 
    // Переопределяем метод OnInit()
    private void ApplyBonuses()
    {
        // Добавляем в список компоненты типа BonusApplier
        // Из дочерних объектов текущего объекта бонусов персонажа
        _bonusAppliers.AddRange(GetComponentsInChildren<BonusApplier>());

        // Проходим по объектам применения бонусов
        for (int i = 0; i < _bonusAppliers.Count; i++)
        {
            // Вызываем у каждого метод ApplyBonus()
            _bonusAppliers[i].ApplyBonus(_existingBonusTypes, gameObject);
        }
    }
    protected override void OnInit()
    {
        // Очищаем список собранных бонусов
        _existingBonusTypes.Clear();

        // Получаем кастомные свойства контроллера в сетевой игре
        ExitGames.Client.Photon.Hashtable props = PhotonView.Controller.CustomProperties;

        // Проходим по всем свойствам
        for (int i = 0; i < props.Count; i++)
        {
            // Пытаемся получить значение свойства
            // Под ключом BonusType с индексом i
            var element = props[$"BonusType{i}"];

            // Если значение найдено (не равно null)
            if (element != null)
            {
                // Добавляем тип бонуса в список
                _existingBonusTypes.Add((BonusType)element);
            }
        }
        // Вызываем метод ApplyBonuses()
        ApplyBonuses();
    }
}