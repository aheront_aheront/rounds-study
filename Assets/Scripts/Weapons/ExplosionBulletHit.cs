﻿using UnityEngine;

// Наследуем ExplosionBulletHit от BulletHit
public class ExplosionBulletHit : BulletHit
{
    // Константа со значением диапазона взрыва
    private const float HitRange = 1f;

    // Конструктор класса ExplosionBulletHit
    // Принимает тип попадания и префаб
    // И передаёт их в базовый класс
    public ExplosionBulletHit(int finalHitType, GameObject hitPrefab) : base(finalHitType, hitPrefab) { }

    // Переопределяем метод Hit()
    // НОВОЕ: Переписали заголовок метода
    public override HitEffectProperties Hit(Collision collision, Transform bulletTransform)
    {
        Collider[] collidersInRange = Physics.OverlapSphere(collision.contacts[0].point, HitRange * FinalHitType);
        CheckCharacterHit(collidersInRange);
        CheckPhysicObjectHit(collidersInRange, bulletTransform, collision.contacts[0].point);

        // НОВОЕ: Возвращаем свойства эффекта попадания
        return new HitEffectProperties(collision.contacts[0].point, Quaternion.identity);
    }
    // Переопределяем метод SpawnHitEffect()
    public override void SpawnHitEffect(HitEffectProperties properties)
    {
        // Создаём объект с визуальными эффектами попадания —
        // В заданной точке и с вращением
        GameObject hitSample = GameObject.Instantiate(HitPrefab, properties.Point, properties.Rotation);

        // Изменяем размер взрыва
        // В зависимости от бонуса
        hitSample.transform.localScale = Vector3.one * FinalHitType;
    }
    // Проверяем попадание в персонажа
    protected bool CheckCharacterHit(Collider[] colliders)
    {
        // Снимаем флаг попадания
        bool isHit = false;

        // Проходим по коллайдерам
        for (int i = 0; i < colliders.Length; i++)
        {
            // Если коллайдер на персонаже
            if (CheckCharacterHit(colliders[i]))
            {
                // Ставим флаг попадания
                isHit = true;
            }
        }
        // Возвращаем значение флага
        return isHit;
    }
    // Проверяем попадание в физический объект
    private bool CheckPhysicObjectHit(Collider[] colliders, Transform bulletTransform, Vector3 point)
    {
        // Снимаем флаг попадания
        bool isHit = false;

        // Проходим по коллайдерам
        for (int i = 0; i < colliders.Length; i++)
        {
            // Вычисляем направление от точки взрыва
            // До ближайшей точки на коллайдере
            Vector3 direction = (colliders[i].ClosestPoint(point) - point).normalized;

            // Если коллайдер на физическом объекте
            if (CheckPhysicObjectHit(colliders[i], direction, point))
            {
                // Ставим флаг попадания
                isHit = true;
            }
        }
        // Возвращаем значение флага
        return isHit;
    }
}