﻿using UnityEngine;

// Наследуем DefaultBulletHit от BulletHit
public class DefaultBulletHit : BulletHit
{
    // Конструктор класса DefaultBulletHit
    // Принимает тип попадания и префаб
    // И передаёт их в базовый класс
    public DefaultBulletHit(int finalHitType, GameObject hitPrefab) : base(finalHitType, hitPrefab) { }

    // Переопределяем метод Hit()
    // НОВОЕ: Переписали заголовок метода
    public override HitEffectProperties Hit(Collision collision, Transform bulletTransform)
    {
        bool isCharacterHit = CheckCharacterHit(collision.collider);
        CheckPhysicObjectHit(collision.collider, bulletTransform.forward, collision.contacts[0].point);
        if (!isCharacterHit)
        {
            Quaternion hitRotation = Quaternion.LookRotation(-bulletTransform.up, -bulletTransform.forward);

            // НОВОЕ: Возвращаем свойства эффекта попадания
            return new HitEffectProperties(collision.contacts[0].point, hitRotation);
        }
        return null;
    }
    // Переопределяем метод SpawnHitEffect()
    public override void SpawnHitEffect(HitEffectProperties properties)
    {
        // Если свойства для создания эффекта не переданы
        if (properties == null)
        {
            // Выходим из метода
            return;
        }
        // Создаём объект с визуальными эффектами попадания —
        // В заданной точке и с вращением
        GameObject.Instantiate(HitPrefab, properties.Point, properties.Rotation);
    }
}