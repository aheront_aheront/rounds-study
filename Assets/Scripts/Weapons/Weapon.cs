﻿// Будем работать с событиями
using System;
using UnityEngine;
// Используем инструменты Photon.Pun
using Photon.Pun;
// И со случайными значениями
using Random = UnityEngine.Random;
// НОВОЕ: Добавили наследование от IHitTypeBonusDependent
public abstract class Weapon : MonoBehaviour, IShootCountBonusDependent, IHitTypeBonusDependent
{
    // Поведение при попадании
    private BulletHit _bulletHit;
    // Урон от оружия
    [SerializeField] private int _damage = 10;

// Префаб пули// Количество выстрелов
    private int _shootCount;
    [SerializeField] private Bullet _bulletPrefab;

// Задержка между выстрелами
    [SerializeField] private float _bulletDelay = 0.05f;

// Количество пуль в магазине
    [SerializeField] private int _bulletsInRow = 7;

// Продолжительность перезарядки
    [SerializeField] private float _reloadingDuration = 4f;

// Точка появления пули
    private Transform _bulletSpawnPoint;
// Переменная для работы с сетевым представлением объекта
    private PhotonView _photonView;
// Текущее количество пуль в магазине
    private int _currentBulletsInRow;

// Счётчик времени между выстрелами
    private float _bulletTimer;

// Счётчик времени перезарядки
    private float _reloadingTimer;

// Флаг окончания задержки
    private bool _isShootDelayEnd;

// Флаг перезарядки
    private bool _isReloading;

// Событие обновления пуль в магазине
    public Action<int, int> OnBulletsInRowChange;

// Событие окончания перезарядки
    public Action OnEndReloading;

// Свойство с данными о перезарядке
// К нему можно будет обращаться из других скриптов
    public bool IsReloading => _isReloading;
    // Устанавливаем поведение пули
    public void SetHit(BulletHit hit)
    {
        // Присваиваем _bulletHit заданное поведение
        _bulletHit = hit;
    }
    
    public void Init()
    {
        _bulletSpawnPoint = GetComponentInChildren<BulletSpawnPoint>().transform;

        // НОВОЕ: Получаем компонент PhotonView
        _photonView = GetComponent<PhotonView>();

        FillBulletsToRow();
    }
// Специальный атрибут
// Для синхронизации действий игроков
    [PunRPC]

// Инициализируем пулю по сети
    public void RPCInitBullet(int bulletViewId)
    {
        // Получаем компонент PhotonView
        PhotonView bulletPhotonView = PhotonNetwork.GetPhotonView(bulletViewId);

        // Получаем компонент Bullet
        Bullet bullet = bulletPhotonView.GetComponent<Bullet>();

        // Вызываем у пули метод Init()
        bullet.Init(_damage, _bulletHit);
    }
    public void SetActive(bool value)
    {
        // Меняем активность оружия на value
        gameObject.SetActive(value);

        // Вызываем событие OnBulletsInRowChange
        // Передаём в него текущее и начальное число пуль
        OnBulletsInRowChange?.Invoke(_currentBulletsInRow, _bulletsInRow);
    }
    public void Shoot()
    {
        // Если задержка между выстрелами не закончилась
        // Или в магазине нет пуль
        if (!_isShootDelayEnd || !CheckHasBulletsInRow())
        {
            // Выходим из метода
            return;
        }
        // Обнуляем таймер выстрела
        _bulletTimer = 0;

        // Вызываем метод DoShoot()
        DoShoot();

        // Уменьшаем количество пуль
        _currentBulletsInRow--;

        // Вызываем событие OnBulletsInRowChange
        // Передаём в него текущее и начальное число пуль
        OnBulletsInRowChange?.Invoke(_currentBulletsInRow, _bulletsInRow);
    }

    public void Reload()
    {
        // Если оружие перезаряжается
        if (_isReloading)
        {
            // Выходим из метода
            return;
        }
        // Ставим флаг перезарядки
        _isReloading = true;
    }
    public bool CheckHasBulletsInRow()
    {
        // Вычисляем текущее количество пуль
        // Если оно > 0, возвращаем true
        return _currentBulletsInRow > 0;
    }

    protected void DoShoot()
    {
        // НОВОЕ: Проходим по количеству выстрелов
        for (int i = 0; i < _shootCount; i++)
        {
            // НОВОЕ: Передаём в SpawnBullet() новый параметр
            // Угол, под которым нужно выстрелить пулю
            SpawnBullet(_bulletPrefab, _bulletSpawnPoint, GetShootAngle(i, _shootCount));
        }
    }

    // НОВОЕ: Добавили новый параметр
    private void SpawnBullet(Bullet prefab, Transform spawnPoint, float extraAngle)
    {
        GameObject bulletGO = PhotonNetwork.Instantiate(prefab.name, spawnPoint.position, spawnPoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();

        Vector3 bulletEulerAngles = bullet.transform.eulerAngles;
        bulletEulerAngles.x += extraAngle;
        bullet.transform.eulerAngles = bulletEulerAngles;

        // НОВОЕ: Получаем компонент PhotonView
        PhotonView bulletPhotonView = bullet.GetComponent<PhotonView>();

        // НОВОЕ: Вызываем RPC для инициализации пули
        _photonView.RPC(nameof(RPCInitBullet), RpcTarget.All, bulletPhotonView.ViewID);
    }
    public void SetShootCount(int value)
    {
        // Приравниваем количество выстрелов к value
        _shootCount = value;
    }

    private float GetShootAngle(int shootId, int shootCount)
    {
        // Начальный угол стрельбы
        float startAngle = 0;

        // Шаг изменения угла
        float stepAngle = 0;

        // Выбираем действие в зависимости от числа выстрелов
        switch (shootCount)
        {
            // Если выстрелов два
            case 2:

                // Начальный угол приравниваем к -3 градусам
                startAngle = -3;

                // Шаг угла изменения приравниваем к 6 градусам
                stepAngle = 6;

                // Выходим из выбора
                break;

            // Если выстрелов три
            case 3:

                // Начальный угол приравниваем к -5 градусам
                startAngle = -5;

                // Шаг угла изменения приравниваем к 5 градусам
                stepAngle = 5;

                // Выходим из выбора
                break;

            // Если выстрелов четыре
            case 4:

                // Начальный угол приравниваем к -6 градусам
                startAngle = -6;

                // Шаг угла изменения приравниваем к 4 градусам
                stepAngle = 4;

                // Выходим из выбора
                break;

            // Если выстрелов другое количество
            default:

                // Возвращаем угол 0
                return 0;
        }
        // Возвращаем угол для конкретного выстрела
        // Считаем его как начальный угол плюс произведение
        // Шага угла и порядкового номера выстрела
        return startAngle + stepAngle * shootId;
    }

  
    private void Update()
    {
        // Вызываем метод ShootDelaying()
        ShootDelaying();

        // Вызываем метод Reloading()
        Reloading();
    }

    private void ShootDelaying()
    {
        // Увеличиваем таймер выстрела
        // На время, прошедшее с последнего кадра
        _bulletTimer += Time.deltaTime;

        // Присваиваем _isShootDelayEnd значение
        // В зависимости от того, прошла ли задержка
        _isShootDelayEnd = _bulletTimer >= _bulletDelay;
    }
    private void Reloading()
    {
        // Если оружие перезаряжается
        if (_isReloading)
        {
            // Увеличиваем таймер перезарядки
            // На время, прошедшее с последнего кадра
            _reloadingTimer += Time.deltaTime;

            // Если прошла продолжительность перезарядки
            if (_reloadingTimer >= _reloadingDuration)
            {
                // Вызываем метод FillBulletsToRow()
                FillBulletsToRow();

                // Вызываем событие OnEndReloading
                OnEndReloading?.Invoke();
            }
        }
    }

    private void FillBulletsToRow()
    {
        // Снимаем флаг перезарядки
        _isReloading = false;

        // Обнуляем таймер перезарядки
        _reloadingTimer = 0;

        // Задаём текущее количество пуль
        _currentBulletsInRow = _bulletsInRow;

        // Вызываем событие OnBulletsInRowChange
        // Передаём в него текущее и начальное число пуль
        OnBulletsInRowChange?.Invoke(_currentBulletsInRow, _bulletsInRow);
    }
    
}