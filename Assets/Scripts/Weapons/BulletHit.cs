﻿using UnityEngine;

// Сделали класс абстрактным
// Убрали наследование от MonoBehaviour
public abstract class BulletHit
{
    // Префаб, который будет создан при попадании пули
    protected GameObject HitPrefab;

    // Тип попадания
    protected int FinalHitType;

    // Урон от пули
    protected int Damage;

    // Импульс пули
    protected float Impulse;

    // Конструктор класса BulletHit
    // Принимает тип попадания и префаб
    public BulletHit(int finalHitType, GameObject hitPrefab)
    {
        // Задаём тип попадания
        FinalHitType = finalHitType;

        // Задаём префаб
        HitPrefab = hitPrefab;
    }
    // Инициализируем переменные
    public void Init(int damage, float impulse)
    {
        // Задаём урон
        Damage = damage;

        // Задаём импульс
        Impulse = impulse;
    }
    // НОВОЕ: Сделали метод абстрактным, задали второй параметр
    public abstract HitEffectProperties Hit(Collision collision, Transform bulletTransform);
    // Создаём визуальные эффекты попадания
    public abstract void SpawnHitEffect(HitEffectProperties properties);

// НОВОЕ: Заменили параметр Collision на Collider
    protected bool CheckCharacterHit(Collider collider)
    {
        CharacterHealth hitedHealth = collider.GetComponentInParent<CharacterHealth>();

        if (hitedHealth)
        {
            hitedHealth.AddHealthPoints(-Damage);
            return true;
        }
        return false;
    }
// НОВОЕ: Задали три новых параметра
    protected bool CheckPhysicObjectHit(Collider collider, Vector3 direction, Vector3 point)
    {
        IPhysicHittable hittedPhysicObject = collider.GetComponentInParent<IPhysicHittable>();

        if (hittedPhysicObject != null)
        {
            hittedPhysicObject.Hit(direction * Impulse, point);
            return true;
        }
        return false;
    }
}