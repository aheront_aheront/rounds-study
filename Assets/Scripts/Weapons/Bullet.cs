﻿using UnityEngine;
// Используем инструменты Photon.Pun
using Photon.Pun;
// Сделали класс абстрактным
public abstract class Bullet : MonoBehaviour
{
  
// Импульс (сила толчка) пули
    [SerializeField] private float _impulse = 30f;
// Переменная для работы с сетевым представлением объекта
    private PhotonView _photonView;
// Максимальное время отображения пули
    [SerializeField] private float _lifeTime = 15f;
// Поведение при попадании
    private BulletHit _hit;
    private void Awake()
    {
        // Получаем компонент PhotonView
        _photonView = GetComponent<PhotonView>();

        // Если у игрока его нет
        if (!_photonView.IsMine)
        {
            // Уничтожаем компонент физики на персонаже
            // Чтобы физика пули отрабатывалась на клиенте, который создал пулю
            // Её позиция будет синхронизироваться с другими клиентами
            Destroy(GetComponent<Rigidbody>());
        }
    }
    // НОВОЕ: Добавили два параметра
// Урон и поведение при попадании
    public void Init(int damage, BulletHit hit)
    {
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        rigidbody.AddForce(transform.forward * _impulse, ForceMode.Impulse);

        // НОВОЕ: Присваиваем _hit заданное поведение
        _hit = hit;

        // НОВОЕ: Вызываем у поведения метод Init()
        // Передаём в него урон и импульс пули
        hit.Init(damage, _impulse);
    }
    private void Update()
    {
        // НОВОЕ: Если у игрока нет PhotonView
        if (!_photonView.IsMine)
        {
            // НОВОЕ: Выходим из метода
            return;
        }
        ReduceLifeTime();
    }

    private void ReduceLifeTime()
    {
        // Сокращаем время отображения пули
        // На время, прошедшее с последнего кадра
        _lifeTime -= Time.deltaTime;

        // Если время отображения пули истекло
        if (_lifeTime <= 0)
        {
            // Вызываем метод DestroyBullet()
            DestroyBullet();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!_photonView.IsMine)
        {
            return;
        }
        if (collision.collider)
        {
            // НОВОЕ: Получаем свойства эффекта попадания
            HitEffectProperties hitEffectProps = _hit.Hit(collision, transform);

            // НОВОЕ: Если они существуют
            if (hitEffectProps != null)
            {
                // НОВОЕ: Сериализуем свойства в JSON
                string hitEffectPropsJson = JsonUtility.ToJson(hitEffectProps);
            
                // НОВОЕ: Отправляем эти данные с помощью RPC
                _photonView.RPC(nameof(RPCSpawnHitEffect), RpcTarget.All, hitEffectPropsJson);
            }
            DestroyBullet();
        }
    }
    private void DestroyBullet()
    {
        // НОВОЕ: Удаляем объект пули через PhotonNetwork
        PhotonNetwork.Destroy(gameObject);
        
        
    }
    
    // Специальный атрибут
// Для синхронизации действий игроков
    [PunRPC]

// Создаём эффект появления пули по сети
    public void RPCSpawnHitEffect(string propertiesJson)
    {
        // Десериализуем JSON в объект свойств эффекта попадания
        HitEffectProperties properties = JsonUtility.FromJson<HitEffectProperties>(propertiesJson);

        // Создаём эффект попадания с этими свойствами
        _hit.SpawnHitEffect(properties);
    }   
}