using UnityEngine;
using Photon.Pun;

public class PlayerSpawner : MonoBehaviour
{
    [SerializeField] private Player _playerPrefab;

    // НОВОЕ: Префаб шкалы здоровья игрока
    [SerializeField] private PlayerGamePanel _playerGamePanelPrefab;

    private PhotonView _photonView;
    private Location _location;
    private Character _localPlayer;
// Свойство для доступа к объекту локального игрока
    public Character LocalPlayer => _localPlayer;
    // НОВОЕ: Трансформа родительского объекта шкалы
    private Transform _playerGamePanelsParent;

    // НОВОЕ: Объединили методы Awake() и Start()
    // Под именем Init()
    public void Init()
    {
        _photonView = GetComponent<PhotonView>();
        _location = FindObjectOfType<Location>();

        // НОВОЕ: Получаем трансформу родительского объекта
        _playerGamePanelsParent = ScreensController.Current.GetScreen<GameScreen>().transform;

        if (_photonView.IsMine)
        {
            SpawnPlayer();
        }
    }

    public void ActivatePlayer()
    {
        _localPlayer.Activate();
    }

    private void SpawnPlayer()
    {
        PlayerSpawnPoint spawnPoint = _location.SpawnPoints[PhotonNetwork.LocalPlayer.ActorNumber - 1];

        // НОВОЕ: Создаём игрока в заданной точке
        GameObject playerGO = PhotonNetwork.Instantiate(_playerPrefab.name, spawnPoint.transform.position, Quaternion.identity);

        // НОВОЕ: Получаем PhotonView этого игрока
        PhotonView playerPhotonView = playerGO.GetComponent<PhotonView>();

        // НОВОЕ: Вызываем сетевую функцию
        // Для инициализации всех копий игрока в сети
        _photonView.RPC(nameof(RPCInitPlayer), RpcTarget.All, playerPhotonView.ViewID);
    }
    // Специальный атрибут
// Для синхронизации действий игроков
    [PunRPC]
// Инициализируем игрока по сети
    private void RPCInitPlayer(int playerPhotonViewId)
    {
        // Получаем PhotonView экземпляра игрока по его id
        PhotonView playerPhotonView = PhotonNetwork.GetPhotonView(playerPhotonViewId);

        // Получаем компонент Character
        Character player = playerPhotonView.GetComponent<Player>();

        // Инициализируем игрока
        player.Init();

        // Создаём для него шкалу здоровья
        SpawnPlayerGamePanel(player);

        // Останавливаем игрока
        // Например, если он должен ждать старта игры
        player.Stop();

        // Если PhotonView принадлежит текущему игроку
        if (playerPhotonView.IsMine)
        {
            // Сохраняем ссылку на этого игрока
            _localPlayer = player;
        }
    }
// Создаём шкалу здоровья игрока
    private void SpawnPlayerGamePanel(Character player)
    {
        // Получаем компонент CharacterHealth
        CharacterHealth health = player.GetComponent<CharacterHealth>();

        // Создаём шкалу здоровья
        PlayerGamePanel gamePanel = Instantiate(_playerGamePanelPrefab, Vector3.zero, Quaternion.identity);

        // Устанавливаем её в родительский объект
        gamePanel.transform.SetParent(_playerGamePanelsParent);

        // Задаём цель
        gamePanel.SetTarget(health);
    }
    // Останавливаем игрока
    public void StopPlayer()
    {
        // Вызываем у него метод Stop()
        _localPlayer.Stop();
    }
}