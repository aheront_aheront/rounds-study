// Используем команды Unity
using UnityEngine;

// Работаем с инструментами Photon.Pun
using Photon.Pun;// Наследуем GameStateChanger от BaseStateChanger// Используем коллекции
using System.Collections.Generic;
public class GameStateChanger : BaseStateChanger
{
    // Префаб объекта появления игроков
    [SerializeField] private PlayerSpawner _playerSpawnerPrefab;

// Игровая локация
    [SerializeField] private Location _location;
// Константа, задающая количество бонусов для выбора
    private const int BonusForChooseCount = 3;

// Объект типа BonusDataAccesser
    private BonusDataAccesser _bonusDataAccesser;

// Объект типа PlayerBonusesAccesser
    private PlayerBonusesAccesser _playerBonusAccesser;
// Переменная для работы с сетевым представлением объекта
    private PhotonView _photonView;

// Локальный объект появления игроков
    private PlayerSpawner _localPlayerSpawner;

// Количество игроков, готовых к игре
    public static int ReadyCount;

// Количество инициализированных локаций
    private int _locationSpawnCount;
    public void AfterLocationSpawn()
    {
        // Отправляем RPC-сообщения всем игрокам
        // Чтобы обработать завершение создания локации
        _photonView.RPC(nameof(RPCAfterLocationSpawn), RpcTarget.All);
    }

    protected override void OnInit()
    {
        RefreshWaitScreen();
        _locationSpawnCount = 0;
        _photonView = GetComponent<PhotonView>();

        // НОВОЕ: Находим компонент BonusDataAccesser
        _bonusDataAccesser = FindObjectOfType<BonusDataAccesser>();

        // НОВОЕ: Создаём объект PlayerBonusesAccesser
        _playerBonusAccesser = new PlayerBonusesAccesser();

        // НОВОЕ: Инициализируем его
        _playerBonusAccesser.Init();

        // НОВОЕ: Показываем экран выбора бонусов
        BonusChooseScreen bonusChooseScreen = ScreensController.ShowScreen<BonusChooseScreen>();

        // НОВОЕ: Обрабатываем событие OnOkButtonClick
        // Вызываем метод BonusSelected()
        bonusChooseScreen.OnOkButtonClick += BonusSelected;

        // НОВОЕ: Получаем случайные бонусы
        List<BonusData> randomBonuses = _bonusDataAccesser.GetRandomBonuses(_playerBonusAccesser.ExistingBonusTypes, BonusForChooseCount);

        // НОВОЕ: Отображаем случайные бонусы
        bonusChooseScreen.ShowBonuses(randomBonuses);

        CountDownScreen countDownScreen = ScreensController.GetScreen<CountDownScreen>();
        countDownScreen.OnCountDownEnd += StartGame;
        // Получаем экран EndRoundScreen
        EndRoundScreen endRoundScreen = ScreensController.GetScreen<EndRoundScreen>();

// Обрабатываем событие OnGiveUpButtonClick
// Вызываем метод LeaveGame()
        endRoundScreen.OnGiveUpButtonClick += LeaveGame;

// Обрабатываем событие OnPlayButtonClick
// Вызываем метод NextRound()
        endRoundScreen.OnPlayButtonClick += NextRound;

// Получаем экран EndGameScreen
        EndGameScreen endGameScreen = ScreensController.GetScreen<EndGameScreen>();

// Обрабатываем событие OnLeaveButtonClick
// Вызываем метод LeaveGame()
        endGameScreen.OnLeaveButtonClick += LeaveGame;

// Обрабатываем событие OnDieWithObject
// Вызываем метод SetLoser()
        CharacterHealth.OnDieWithObject += SetLoser;
    }
    // Вызывается при выборе бонуса
    private void BonusSelected(BonusType? selectedType)
    {
        // Если бонус выбран
        if (selectedType.HasValue)
        {
            // Добавляем выбранный бонус
            _playerBonusAccesser.AddBonus(selectedType.Value);
        }
        // Показываем экран ожидания
        ScreensController.ShowScreen<WaitScreen>();

        // Вызываем сетевую функцию готовности
        _photonView.RPC(nameof(RPCSendReady), RpcTarget.All);
    }
    // Специальный атрибут
// Для синхронизации действий игроков
    [PunRPC]
    private void RPCSendReady()
    {
        // НОВОЕ: Увеличиваем счётчик готовых игроков на 1
        ReadyCount++;

        // НОВОЕ: Вызываем метод RefreshWaitScreen()
        RefreshWaitScreen();

        // НОВОЕ: Заменили приватную переменную на публичную
        if (ReadyCount >= PhotonNetwork.CurrentRoom.MaxPlayers)
        {
            // НОВОЕ: Обнуляем счётчик готовых игроков
            ReadyCount = 0;

            PrepareGame();
        }
    }
    private void RefreshWaitScreen()
    {
        // Вызываем перегруженный одноимённый метод
        // Передаём в него текущее число готовых игроков
        // И максимальное число игроков в текущей комнате
        RefreshWaitScreen(ReadyCount, PhotonNetwork.CurrentRoom.MaxPlayers);
    }
    private void RefreshWaitScreen(int current, int max)
    {
        // Получаем экран ожидания
        WaitScreen waitScreen = ScreensController.GetScreen<WaitScreen>();

        // Устанавливаем там текст
        // С текущим и максимальным числом игроков
        waitScreen.SetCountText(current, max);
    }
    private void PrepareGame()
    {
        // Если текущий клиент — мастер-клиент
        if (PhotonNetwork.IsMasterClient)
        {
            // Вызываем метод SpawnLocation()
            SpawnLocation();
        }
    }
    private void SpawnLocation()
{
    // Если текущий клиент — мастер-клиент
    if (PhotonNetwork.IsMasterClient)
    {
        // Мастер-клиент создаёт объект в игре
        // Который виден всем игрокам в комнате
        PhotonNetwork.InstantiateRoomObject(_location.name, Vector3.zero, Quaternion.identity, 0, new object[]{ _photonView.ViewID});
    }
}
// Специальный атрибут
// Для синхронизации действий игроков
[PunRPC]
private void RPCAfterLocationSpawn()
{
    // Увеличиваем счётчик созданных локаций
    _locationSpawnCount++;

    // Если количество созданных локаций
    // >= максимальному числу всех игроков в комнате
    if (_locationSpawnCount >= PhotonNetwork.CurrentRoom.MaxPlayers)
    {
        // Вызываем метод SpawnPlayerSpawner()
        SpawnPlayerSpawner();

        // Показываем экран с обратным отсчётом
        CountDownScreen countDownScreen = ScreensController.ShowScreen<CountDownScreen>();

        // Начинаем обратный отсчёт на экране
        countDownScreen.StartCountDown();
    }
}
private void SpawnPlayerSpawner()
{
    // Создаём объект появления игроков
    GameObject playerSpawnerGO = PhotonNetwork.Instantiate(_playerSpawnerPrefab.name, Vector3.zero, Quaternion.identity);

    // Получаем у него компонент PhotonView
    PhotonView spawnerPhotonView = playerSpawnerGO.GetComponent<PhotonView>();

    // Инициализируем объект появления игроков
    // Для всех клиентов через сеть
    _photonView.RPC(nameof(RPCInitPlayerSpawner), RpcTarget.All, spawnerPhotonView.ViewID);
}
// Специальный атрибут
// Для синхронизации действий игроков
[PunRPC]
private void RPCInitPlayerSpawner(int spawnerPhotonViewId)
{
    // Получаем объект PhotonView с указанным ID
    PhotonView spawnerPhotonView = PhotonNetwork.GetPhotonView(spawnerPhotonViewId);

    // Получаем компонент PlayerSpawner
    PlayerSpawner playerSpawner = spawnerPhotonView.GetComponent<PlayerSpawner>();

    // Инициализируем его
    playerSpawner.Init();

    // Если этот PhotonView принадлежит текущему игроку
    if (spawnerPhotonView.IsMine)
    {
        // Сохраняем ссылку на его PlayerSpawner
        _localPlayerSpawner = playerSpawner;
    }
}
private void StartGame()
{
    // Отображаем экран игры
    ScreensController.ShowScreen<GameScreen>();

    // Активируем игрока — делаем его управляемым
    _localPlayerSpawner.ActivatePlayer();
}
// Вызывается при выходе из комнаты
public override void OnLeftRoom()
{
    // Удаляем бонусы
    _playerBonusAccesser.ClearBonuses();

    // Загружаем главное меню
    ScenesLoader.LoadMainMenu();
}
// Вызывается при выходе указанного игрока из комнаты
public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
{
    // Если вышедший игрок — это не локальный игрок
    if (otherPlayer != PhotonNetwork.LocalPlayer)
    {
        // Показываем экран EndGameScreen
        ScreensController.ShowScreen<EndGameScreen>();
    }
}
// Завершаем игру
private void LeaveGame()
{
    // Выходим из комнаты Photon
    PhotonNetwork.LeaveRoom();

    // Показываем экран LoadingScreen
    ScreensController.ShowScreen<LoadingScreen>(false);
}
// Переходим на следующий раунд
private void NextRound()
{
    // Загружаем сцену этого раунда
    ScenesLoader.LoadGame();
}
// Устанавливаем проигравшего
private void SetLoser(CharacterHealth health)
{
    // Отписываемся от события OnDieWithObject
    CharacterHealth.OnDieWithObject -= SetLoser;

    // Отправляем эти данные с помощью RPC
    _photonView.RPC(nameof(RPCSetLoser), RpcTarget.All, health.PhotonView.Controller.ActorNumber);
}
// Специальный атрибут
// Для синхронизации действий игроков
[PunRPC]

// Устанавливаем проигравшего по сети
private void RPCSetLoser(int loseActorNumber)
{
    // Останавливаем действия локального игрока
    _localPlayerSpawner.StopPlayer();

    // Проверяем, равен ли номер локального игрока
    // Номеру, который получили в методе
    bool lose = PhotonNetwork.LocalPlayer.ActorNumber == loseActorNumber;

    // Показываем экран EndRoundScreen
    EndRoundScreen endRoundScreen = ScreensController.ShowScreen<EndRoundScreen>();

    // Устанавливаем результат игры
    // (Проиграл ли локальный игрок или нет)
    // На экране окончания раунда
    endRoundScreen.SetResult(lose);
}
private void OnDestroy()
{
    if (!ScreensController)
    {
        return;
    }
    // НОВОЕ: Получаем экран выбора бонусов
    BonusChooseScreen bonusChooseScreen = ScreensController.GetScreen<BonusChooseScreen>();

    // НОВОЕ: Если он существует
    if (bonusChooseScreen)
    {
        // НОВОЕ: Отписываемся от события OnOkButtonClick
        bonusChooseScreen.OnOkButtonClick -= BonusSelected;
    }
    CountDownScreen countDownScreen = ScreensController.GetScreen<CountDownScreen>();
    if (countDownScreen)
    {
        countDownScreen.OnCountDownEnd -= StartGame;
    }
    // Получаем экран EndGameScreen
    EndGameScreen endGameScreen = ScreensController.GetScreen<EndGameScreen>();

// Если он существует
    if (endGameScreen)
    {
        // Отписываемся от события OnLeaveButtonClick
        endGameScreen.OnLeaveButtonClick -= LeaveGame;
    }
// Отписываемся от события OnDieWithObject
    CharacterHealth.OnDieWithObject -= SetLoser;
}
}
