﻿// Используем инструменты Photon.Pun
using Photon.Pun;

// Сделали класс абстрактным
// Наследуем его от MonoBehaviourPunCallbacks
public abstract class BaseStateChanger : MonoBehaviourPunCallbacks
{
    // Защищённое свойство ScreensController
    // Доступно вне класса только для чтения
    // При этом его можно менять внутри класса и его наследников
    protected ScreensController ScreensController { get; private set; }

    // Защищённый абстрактный метод OnInit()
    protected abstract void OnInit();

    // Вызывается при запуске игры
    private void Start()
    {
        // Вызываем метод Init()
        Init();
    }
    // Инициализируем переменные
    private void Init()
    {
        // Находим объект типа ScreensController
        ScreensController = FindAnyObjectByType<ScreensController>();

        // Инициализируем его
        ScreensController.Init();

        // Вызываем метод OnInit()
        OnInit();
    }
}