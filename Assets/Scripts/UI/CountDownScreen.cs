﻿// Используем события
using System;

// Работаем с элементами TMPro
using TMPro;

// Используем команды Unity
using UnityEngine;
// Наследуем CountDownScreen от BaseScreen
public class CountDownScreen : BaseScreen
    {
        // Константа начального значения отсчёта
        private const int StartCountDownValue = 3;

// Надпись про обратный отсчёт
        [SerializeField] private TextMeshProUGUI _countDownText;

// Задержка между числами в секундах
        [SerializeField] private float _secondDelay = 1.3f;

// Задержка перед появлением текста
        [SerializeField] private float _startTextDelay = 0.3f;

// Таймер обратного отсчёта
        private float _timer;

// Флаг активности отсчёта
        private bool _isActive;

// Текущее значение отсчёта
        private int _countDownValue;

// Текущая задержка
        private float _currentDelay;

// Событие завершения отсчёта
        public Action OnCountDownEnd;
        
        public void StartCountDown()
        {
                // Активируем отсчёт
                _isActive = true;

                // Вызываем метод SetCountDownValue()
                SetCountDownValue(StartCountDownValue);
        }
        private void SetCountDownValue(int value)
        {
                // Запоминаем текущее значение отсчёта
                _countDownValue = value;

                // Определяем текущую задержку
                // В зависимости от значения value
                _currentDelay = value > 0 ? _secondDelay : _startTextDelay;

                // Вызываем метод SetCountDownText()
                SetCountDownText(value);

                // Если отсчёт окончен
                if (_countDownValue <= -1)
                {
                        // Вызываем метод EndCountDown()
                        EndCountDown();
                }
        }
        private void SetCountDownText(int value)
        {
                // Отображаем число (значение отсчёта)
                // Или слово «Начали!»
                _countDownText.text = value > 0 ? $"{value}" : "Начали!";
        }
        private void Update()
        {
                // Если отсчёт не активен
                if (!_isActive)
                {
                        // Выходим из метода
                        return;
                }
                // Вызываем метод TimerTick()
                TimerTick();
        }
        private void TimerTick()
        {
                // Увеличиваем значение таймера
                // На время, прошедшее между кадрами
                _timer += Time.deltaTime;

                // Если текущее время превысило время задержки
                if (_timer >= _currentDelay)
                {
                        // Уменьшаем значение таймера
                        // На значение задержки
                        _timer -= _currentDelay;

                        // Уменьшаем значение отсчёта
                        _countDownValue--;

                        // Вызываем метод SetCountDownValue()
                        SetCountDownValue(_countDownValue);
                }
        }
        private void EndCountDown()
        {
                // Вызываем событие OnCountDownEnd
                OnCountDownEnd?.Invoke();
        }
    }
