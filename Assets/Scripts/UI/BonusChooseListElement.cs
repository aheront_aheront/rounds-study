﻿// Используем команды Unity
using UnityEngine;

// Работаем с объектами UI
using UnityEngine.UI;

// Используем элементы TMPro
using TMPro;

// Работаем с событиями
using System;

public class BonusChooseListElement : MonoBehaviour
{
    // Фоновая картинка
    [SerializeField] private Image _backImage;

    // Надпись с заголовком
    [SerializeField] private TextMeshProUGUI _titleText;

    // Кнопка выбора
    [SerializeField] private Button _selectButton;

    // Выбранный цвет
    [SerializeField] private Color _selectedColor;

    // Не выбранный цвет
    [SerializeField] private Color _unselectedColor;

    // Тип бонуса
    private BonusType _bonusType;

    // Событие нажатия на кнопку выбора
    public Action<BonusChooseListElement> OnSelectButtonClick;

    // Свойство для доступа к типу бонуса
    public BonusType BonusType => _bonusType;

    // Устанавливаем бонус
    public void SetBonus(BonusType bonusType, string title)
    {
        // Задаём тип
        _bonusType = bonusType;

        // Задаём заголовок
        _titleText.text = title;
    }
    // Устанавливаем выбранный цвет
    public void SetSelected(bool value)
    {
        // Задаём цвет фоновой картинки
        _backImage.color = value ? _selectedColor : _unselectedColor;
    }
    // Вызывается при запуске игры
    private void Start()
    {
        // Обрабатываем нажатие на кнопку выбора
        _selectButton.onClick.AddListener(SelectButtonClick);
    }
    // Вызывается при нажатии на кнопку выбора
    private void SelectButtonClick()
    {
        // Вызываем событие OnSelectButtonClick
        // Передаём в него текущий элемент
        OnSelectButtonClick?.Invoke(this);
    }
}