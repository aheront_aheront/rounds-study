using System.Collections;
// Используем команды Unity
using UnityEngine;

// Работаем с событиями
using System;

// Используем коллекции
using System.Collections.Generic;

// Работаем с подключениями к серверам Photon
using Photon.Realtime;
using System.Collections.Generic;
using UnityEngine;

public class RoomListView : MonoBehaviour
{
    // Префаб элемента списка комнат
    [SerializeField] private RoomListElement _roomListElementPrefab;

// Трансформа контейнера для размещения элементов
    [SerializeField] private Transform _roomListContainer;

// Список комнат
    private List<RoomListElement> _roomListElements = new List<RoomListElement>();

// Событие входа в комнату
    public Action<string> OnJoinRoomButtonClick;
    // Задаём список комнат
    public void SetRoomList(List<RoomInfo> roomList)
    {
        // Вызываем метод ClearContainer()
        ClearContainer();

        // Проходим по списку комнат
        for (int i = 0; i < roomList.Count; i++)
        {
            // Если текущий элемент удалён из списка
            if (roomList[i].RemovedFromList || !roomList[i].IsVisible)
            {
                // Переходим к новому
                continue;
            }
            // Создаём префаб элемента списка комнат
            // Добавляем его в контейнер
            RoomListElement element = Instantiate(_roomListElementPrefab, _roomListContainer);

            // Устанавливаем название комнаты
            element.SetRoomName(roomList[i].Name);

            // Обрабатываем событие OnJoinButtonClick
            // Вызываем метод JoinRoomButtonClick()
            element.OnJoinButtonClick += JoinRoomButtonClick;

            // Добавляем элемент в список комнат
            _roomListElements.Add(element);
        }
    }
// Очищаем контейнер с комнатами
    private void ClearContainer()
    {
        // Проходим по списку комнат в обратном порядке
        for (int i = _roomListElements.Count - 1; i >= 0; i--)
        {
            // Удаляем каждую комнату
            Destroy(_roomListElements[i].gameObject);
        }
        // Очищаем список комнат
        _roomListElements.Clear();
    }
// Вызывается при нажатии на кнопку входа в комнату
    private void JoinRoomButtonClick(string roomName)
    {
        // Вызываем событие OnJoinRoomButtonClick
        // Передаём в него комнату с заданным именем
        OnJoinRoomButtonClick?.Invoke(roomName);
    }
}
