﻿
// Используем элементы TMPro
using TMPro;

// Работаем с командами Unity
using UnityEngine;
    public class PlayerGamePanel : MonoBehaviour
    {
        // Надпись с именем игрока
        [SerializeField] private TextMeshProUGUI _playerNameText;

// Трансформа отображения здоровья игрока
        [SerializeField] private Transform _playerHealthPercentsViewTransform;

// Вектор смещения
        [SerializeField] private Vector3 _deltaPosition;

// Здоровье целевого игрока
        private CharacterHealth _target;

// Главная камера
        private Camera _mainCamera;
        public void SetTarget(CharacterHealth target)
        {
                // Если нет целевого игрока
                if (target == null)
                {
                        // Выходим из метода
                        return;
                }
                // Присваиваем _target заданный объект
                _target = target;

                // Обрабатываем событие OnAddHealthPoints
                // Вызываем метод RefreshPlayerHealthView()
                target.OnAddHealthPoints += RefreshPlayerHealthView;

                // Если есть надпись с именем игрока
                if (_playerNameText != null)
                {
                        // Делаем имя игрока равным Owner.NickName
                        _playerNameText.text = target.photonView.Owner.NickName;
                }
        }
        private void RefreshPlayerHealthView()
        {
                // Если есть трансформа отображения здоровья игрока
                // И есть объект целевого игрока
                if (_playerHealthPercentsViewTransform && _target)
                {
                        // Масштабируем индикатор здоровья
                        // В зависимости от текущего здоровья
                        _playerHealthPercentsViewTransform.localScale = new Vector3((float)_target.GetHealthPoints() / _target.GetStartHealthPoints(), 1f, 1f);
                }
        }
        private void Start()
        {
                // Присваиваем _mainCamera объект камеры
                _mainCamera = Camera.main;
        }
        private void Update()
        {
                // Вызываем метод CheckForDestroy()
                CheckForDestroy();

                // Вызываем метод RefreshPosition()
                RefreshPosition();
        }
        private void CheckForDestroy()
        {
                // Если нет целевого игрока
                if (!_target)
                {
                        // Удаляем объект шкалы здоровья
                        Destroy(gameObject);
                }
        }
        private void RefreshPosition()
        {
                // Получаем текущую позицию игрока
                Vector3 targetPosition = _target.transform.position;

                // Преобразуем её из мировых координат в экранные
                targetPosition = _mainCamera.WorldToScreenPoint(targetPosition);

                // Прибавляем к позиции вектор смещения
                transform.position = targetPosition + _deltaPosition;
        }
        private void OnDestroy()
        {
                // Если нет целевого игрока
                if (!_target)
                {
                        // Отписываемся от события OnAddHealthPoints
                        _target.OnAddHealthPoints -= RefreshPlayerHealthView;
                }
        }
    }
