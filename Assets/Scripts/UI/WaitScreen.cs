﻿// Используем элементы TMPro
using TMPro;

// Работаем с командами Unity
using UnityEngine;

// Наследуем WaitScreen от BaseScreen
public class WaitScreen : BaseScreen
{
    // Надпись про готовность игроков
    [SerializeField] private TextMeshProUGUI _countText;

    // Задаём текст надписи
    public void SetCountText(int current, int max)
    {
        // Делаем его равным заданной строке
        _countText.text = $"Готовы {current} из {max}";
    }
}