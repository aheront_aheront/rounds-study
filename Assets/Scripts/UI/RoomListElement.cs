﻿// Используем команды Unity
using UnityEngine;

// Работаем с элементами TMPro
using TMPro;

// Используем элементы UI
using UnityEngine.UI;

// Работаем с событиями
using System;

public class RoomListElement : MonoBehaviour
{
    // Надпись с названием комнаты
    [SerializeField] private TextMeshProUGUI _roomNameText;

    // Кнопка входа в комнату
    [SerializeField] private Button _joinButton;

    // Событие входа в комнату
    public Action<string> OnJoinButtonClick;

    // Название комнаты
    private string _roomName;

    // Задаём название комнаты
    public void SetRoomName(string value)
    {
        // Делаем его равным value
        _roomName = value;

        // Задаём текст надписи
        _roomNameText.text = value;
    }
    // Вызывается при запуске игры
    private void Start()
    {
        // Обрабатываем нажатие на кнопку входа в комнату
        _joinButton.onClick.AddListener(JoinButtonClick);
    }
    // Вызывается при нажатии на кнопку входа в комнату
    private void JoinButtonClick()
    {
        // Вызываем событие OnJoinButtonClick
        // Передаём в него название комнаты
        OnJoinButtonClick?.Invoke(_roomName);
    }
}