// Используем элементы UI

using MainMenu;
using UnityEngine.UI;

// Работаем с командами Unity
using UnityEngine;

// Наследуем LobbyScreen от BaseScreen
public class LobbyScreen : BaseScreen
{
    // Поле ввода имени игрока
    [SerializeField] private PlayerNameInput _playerNameInput;

    // Окно со списком комнат
    [SerializeField] private RoomListView _roomListView;

    // Кнопка создания комнаты
    [SerializeField] private Button _createRoomButton;

    // Свойство для доступа к списку комнат
    public RoomListView RoomListView => _roomListView;

    // Вызывается при запуске игры
    private void Start()
    {
        // Вызываем у поля ввода метод Init()
        _playerNameInput.Init();

        // Обрабатываем нажатие на кнопку создания комнаты
        _createRoomButton.onClick.AddListener(CreateRoomButtonClick);
    }
    // Вызывается при нажатии на кнопку создания комнаты
    private void CreateRoomButtonClick()
    {
        // Отображаем экран создания комнаты
        ScreensController.Current.ShowScreen<CreateRoomScreen>();
    }
}