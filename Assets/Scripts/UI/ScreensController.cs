﻿using System.Collections.Generic;
using UnityEngine;

public class ScreensController : MonoBehaviour
{
    // Массив экранов
    private BaseScreen[] _screens;

// Стек предыдущих экранов
    private Stack<BaseScreen> _prevScreens = new Stack<BaseScreen>();

// Текущий экран
    private BaseScreen _currentScreen;

// Свойство для проверки, есть ли объект текущего экрана в игре
    public static bool HasCurrent => Current;

// Свойство для доступа к текущему экрану
    public static ScreensController Current { get; private set; }
    public void Init()
    {
        // Устанавливаем текущий экран
        Current = this;

        // Получаем все экраны из дочерних объектов
        _screens = GetComponentsInChildren<BaseScreen>(true);

        // Вызываем метод HideAllScreens()
        HideAllScreens();
    }

    public T ShowScreen<T>(bool insertToPrev = true) where T : BaseScreen
    {
        // Если есть текущий экран
        if (_currentScreen)
        {
            // Скрываем его
            _currentScreen.SetActive(false);

            // Если стоит флаг insertToPrev
            // И это не экран загрузки
            if (insertToPrev && !(_currentScreen is LoadingScreen))
            {
                // Добавляем текущий экран в стек предыдущих 
                _prevScreens.Push(_currentScreen);
            }
        }
        // Получаем экран нужного типа
        _currentScreen = GetScreen<T>();

        // Показываем его
        _currentScreen.SetActive(true);

        // Возвращаем экран как результат
        return _currentScreen as T;
    }

    public T GetScreen<T>() where T : BaseScreen
    {
        // Проходим по всем экранам
        for (int i = 0; i < Current._screens.Length; i++)
        {
            // Если найден экран нужного типа
            if (_screens[i] is T targetScreen)
            {
                // Возвращаем его
                return targetScreen;
            }
        }
        // Возвращаем пустое значение
        return null;
    }
    public void ShowPrevScreen()
    {
        // Показываем экран из стека предыдущих
        ShowScreen(_prevScreens.Pop());
    }

    private void HideAllScreens()
    {
        // Проходим по всем экранам
        for (int i = 0; i < Current._screens.Length; i++)
        {
            // Скрываем каждый
            _screens[i].SetActive(false);
        }
    }

    private void ShowScreen(BaseScreen screen)
    {
        // Скрываем текущий экран
        _currentScreen.SetActive(false);

        // Ставим новый текущий экран
        _currentScreen = screen;

        // Показываем его
        _currentScreen.SetActive(true);
    }
}