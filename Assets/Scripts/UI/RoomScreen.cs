// Используем команды Unity
using UnityEngine;

// Работаем с элементами TMPro
using TMPro;

// Используем элементы UI
using UnityEngine.UI;

// Работаем с событиями
using System;

// Наследуем RoomScreen от BaseScreen
public class RoomScreen : BaseScreen
{
    // Кнопка начала игры
    [SerializeField] private Button _playButton;

// Отображение списка игроков
    [SerializeField] private PlayerListView _playerListView;

// Свойство для доступа к списку игроков
    public PlayerListView PlayerListView => _playerListView;

// Событие нажатия на кнопку начала
    public Action OnPlayButtonClick;
    // Надпись с названием комнаты
    [SerializeField] private TextMeshProUGUI _roomNameText;

    // Кнопка выхода из комнаты
    [SerializeField] private Button _leaveRoomButton;

    // Событие выхода из комнаты
    public Action OnLeaveButtonClick;

    // Задаём название комнаты
    public void SetRoomNameText(string value)
    {
        // Делаем его равным value
        _roomNameText.text = value;
    }
    // Вызывается при запуске игры
    private void Start()
    {
        _leaveRoomButton.onClick.AddListener(LeaveButtonClick);

        // НОВОЕ: Обрабатываем нажатие на кнопку начала игры
        _playButton.onClick.AddListener(PlayButtonClick);
    }
    // Задаём активность кнопки начала игры
    public void SetActivePlayButton(bool value)
    {
        // Делаем её равной value
        _playButton.gameObject.SetActive(value);
    }
// Вызывается при нажатии на кнопку начала игры
    private void PlayButtonClick()
    {
        // Вызываем событие OnPlayButtonClick
        OnPlayButtonClick?.Invoke();
    }
    // Вызывается при нажатии на кнопку выхода из комнаты
    private void LeaveButtonClick()
    {
        // Вызываем событие OnLeaveButtonClick
        OnLeaveButtonClick?.Invoke();
    }
}