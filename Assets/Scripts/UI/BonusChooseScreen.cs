﻿
// Используем команды Unity
using UnityEngine;

// Работаем с объектами UI
using UnityEngine.UI;

// Используем события
using System;

// Работаем с коллекциями
using System.Collections.Generic;// Наследуем BonusChooseScreen от BaseScreen
public class BonusChooseScreen : BaseScreen
    {
        // Префаб элемента списка бонусов
        [SerializeField] private BonusChooseListElement _bonusListElementPrefab;

// Кнопка ОК
        [SerializeField] private Button _okButton;

// Трансформа контейнера списка бонусов
        [SerializeField] private Transform _bonusListContainer;

// Объект, который показывает, что все бонусы собраны
        [SerializeField] private GameObject _allBonusTakenGO;

// Список элементов бонусов для выбора
        private List<BonusChooseListElement> _bonusChooseListElements = new List<BonusChooseListElement>();

// Текущий выбранный тип бонуса
        private BonusType? _selectedBonusType;

// Событие нажатия на кнопку ОК
        public Action<BonusType?> OnOkButtonClick;
        public void ShowBonuses(List<BonusData> randomBonuses)
        {
                // Сбрасываем выбранный бонус
                _selectedBonusType = null;

                // Проверяем, есть ли бонусы
                // Задаём значение флага hasBonuses
                bool hasBonuses = randomBonuses.Count == 0;

                // Вызываем метод SetActiveAllBonusTaken()
                // Передаём в него значение флага hasBonuses
                SetActiveAllBonusTaken(hasBonuses);

                // Вызываем метод ClearContainer()
                ClearContainer();

                // Проходим по случайным бонусам
                for (int i = 0; i < randomBonuses.Count; i++)
                {
                        // Добавляем каждый в контейнер
                        AddBonusElement(randomBonuses[i]);
                }
                // Вызываем метод SelectFirstBonusElement()
                SelectFirstBonusElement();
        }
        private void SetActiveAllBonusTaken(bool value)
        {
                // Устанавливаем активность объекта
                // Который сообщает о том, что все бонусы собраны
                _allBonusTakenGO.SetActive(value);
        }
        private void ClearContainer()
        {
                // Проходим по бонусам в обратном порядке
                for (int i = _bonusChooseListElements.Count - 1; i >= 0; i--)
                {
                        // И последовательно отписываемся от события OnSelectButtonClick
                        _bonusChooseListElements[i].OnSelectButtonClick -= SelectElement;

                        // Удаляем каждый элемент
                        Destroy(_bonusChooseListElements[i].gameObject);
                }
                // Очищаем список бонусов
                _bonusChooseListElements.Clear();
        }
        private void AddBonusElement(BonusData bonusData)
        {
                // Создаём экземпляр префаба
                // Элемента списка бонусов
                BonusChooseListElement element = Instantiate(_bonusListElementPrefab, _bonusListContainer);

                // Устанавливаем тип и название бонуса
                element.SetBonus(bonusData.Type, bonusData.Title);

                // Обрабатываем событие OnSelectButtonClick
                // Вызываем метод SelectElement()
                element.OnSelectButtonClick += SelectElement;

                // Добавляем элемент в список выбора
                _bonusChooseListElements.Add(element);
        }
        private void SelectElement(BonusChooseListElement selectedElement)
        {
                // Запоминаем выбранный тип бонуса
                _selectedBonusType = selectedElement.BonusType;

                // Проходим по бонусам в обратном порядке
                for (int i = _bonusChooseListElements.Count - 1; i >= 0; i--)
                {
                        // Создаём элемент, равный текущему
                        BonusChooseListElement element = _bonusChooseListElements[i];

                        // Указываем, выбран ли этот элемент
                        element.SetSelected(element == selectedElement);
                }
        }
        private void SelectFirstBonusElement()
        {
                // Если элементов нет
                if (_bonusChooseListElements.Count <= 0)
                {
                        // Выходим из метода
                        return;
                }
                // Выбираем первый элемент
                SelectElement(_bonusChooseListElements[0]);
        }
        private void Start()
        {
                // Обрабатываем нажатие на кнопку ОК
                _okButton.onClick.AddListener(OkButtonClick);
        }
        private void OkButtonClick()
        {
                // Вызываем событие OnOkButtonClick
                // Передаём в него выбранный тип бонуса
                OnOkButtonClick?.Invoke(_selectedBonusType);
        }
        
    }
