// Используем элементы TMPro
using TMPro;

// Работаем с командами Unity
using UnityEngine;

// Используем элементы UI
using UnityEngine.UI;

// Наследуем ErrorScreen от BaseScreen
public class ErrorScreen : BaseScreen
{
    // Надпись с текстом ошибки
    [SerializeField] private TextMeshProUGUI _errorText;

    // Кнопка ОК
    [SerializeField] private Button _okButton;

    // Задаём текст ошибки
    public void SetErrorText(string value)
    {
        // Делаем его равным value
        _errorText.text = value;
    }
    // Вызывается при запуске игры
    private void Start()
    {
        // Обрабатываем нажатие на кнопку ОК
        _okButton.onClick.AddListener(OkButtonClick);
    }
    // Вызывается при нажатии на кнопку ОК
    private void OkButtonClick()
    {
        // Отображаем предыдущий экран
        ScreensController.Current.ShowPrevScreen();
    }
}