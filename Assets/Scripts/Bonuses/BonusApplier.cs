﻿using System.Collections.Generic;
using UnityEngine;

// НОВОЕ: Сделали класс интерфейсом
public interface BonusApplier
{
    // НОВОЕ: Метод перестал быть абстрактным
    void ApplyBonus(List<BonusType> existingBonusTypes, GameObject root);
}