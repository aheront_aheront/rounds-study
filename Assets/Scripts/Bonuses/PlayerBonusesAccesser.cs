﻿// Используем инструменты Photon.Pun
using Photon.Pun;

// Работаем с коллекциями
using System.Collections.Generic;

// Убрали наследование от MonoBehaviour
public class PlayerBonusesAccesser 
{
    // Список собранных бонусов игрока
    private List<BonusType> _existingBonusTypes = new List<BonusType>();

    // Свойство для доступа к списку собранных бонусов
    public List<BonusType> ExistingBonusTypes => _existingBonusTypes;

    // Инициализируем работу скрипта
    public void Init()
    {
        // Вызываем метод RefreshExistingBonuses()
        RefreshExistingBonuses();
    }
    // Добавляем новый бонус
    public void AddBonus(BonusType bonusType)
    {
        // Помещаем его в список собранных
        _existingBonusTypes.Add(bonusType);

        // Создаём хеш-таблицу для сохранения свойств
        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();

        // Проходим по списку собранных бонусов
        for (int i = 0; i < _existingBonusTypes.Count; i++)
        {
            // Добавляем каждый в хеш-таблицу
            hashtable.Add($"BonusType{i}", _existingBonusTypes[i]);
        }
        // Сохраняем хеш-таблицу
        // Как пользовательские свойства игрока в Photon
        PhotonNetwork.LocalPlayer.SetCustomProperties(hashtable);
    }
    // Обновляем список собранных бонусов
    private void RefreshExistingBonuses()
    {
        // Очищаем его
        _existingBonusTypes.Clear();

        // Получаем пользовательские свойства игрока из Photon
        ExitGames.Client.Photon.Hashtable playerCustomProperties = PhotonNetwork.LocalPlayer.CustomProperties;

        // Проходим по всем элементам в свойствах игрока
        for (int i = 0; i < playerCustomProperties.Count; i++)
        {
            // Получаем бонус под текущим индексом
            var element = playerCustomProperties[$"BonusType{i}"];

            // Если его значение не пустое
            if (element != null)
            {
                // Добавляем этот бонус в список собранных
                _existingBonusTypes.Add((BonusType)element);
            }
        }
    }   
    // Удаляем бонусы
    public void ClearBonuses()
    {
        // Записываем пользовательские свойства локального игрока
        // В переменную playerCustomProperties
        ExitGames.Client.Photon.Hashtable playerCustomProperties = PhotonNetwork.LocalPlayer.CustomProperties;

        // Проходим по этим свойствам
        for (int i = 0; i < playerCustomProperties.Count; i++)
        {
            // Устанавливаем в каждое значение null
            playerCustomProperties[$"BonusType{i}"] = null;
        }
        // Обновляем пользовательские свойства локального игрока
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerCustomProperties);
    }
}