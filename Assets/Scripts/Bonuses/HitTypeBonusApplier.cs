﻿using System.Collections.Generic;
using UnityEngine;

// Добавили наследование от BonusApplier
public class HitTypeBonusApplier : MonoBehaviour, BonusApplier
{
    // Префаб эффекта обычного попадания
    [SerializeField] private GameObject _defaultHitPrefab;

    // Префаб эффекта попадания со взрывом
    [SerializeField] private GameObject _explosionHitPrefab;

    // Применяем бонусы к пуле
    public void ApplyBonus(List<BonusType> existingBonusTypes, GameObject root)
    {
        // Получаем объект типа попадания
        // С учётом собранных игроком бонусов
        BulletHit hit = GetHit(existingBonusTypes);

        // Применяем объект типа попадания к исходному объекту
        Apply(hit, root);
    }
    // Получаем объект типа попадания
private BulletHit GetHit(List<BonusType> existingBonusTypes)
{
    // Задаём начальную силу взрыва
    int finalHitType = 0;

    // Проходим по собранным игроком бонусам
    for (int i = 0; i < existingBonusTypes.Count; i++)
    {
        // Задаём переменную для типа попадания
        int hitType = 0;

        // Выбираем действие в зависимости от типа бонуса
        switch (existingBonusTypes[i])
        {
            // Если бонус — слабый взрыв
            case BonusType.SmallExplosionHit:

                // Приравниваем hitType к 1
                hitType = 1;

                // Выходим из выбора
                break;

            // Если бонус — средний взрыв
            case BonusType.MediumExplosionHit:

                // Приравниваем hitType к 2
                hitType = 2;

                // Выходим из выбора
                break;

            // Если бонус — сильный взрыв
            case BonusType.LargeExplosionHit:

                // Приравниваем hitType к 3
                hitType = 3;

                // Выходим из выбора
                break;
        }

        // Если начальная силы взрыва < текущей
        if (finalHitType < hitType)
        {
            // Приравниваем начальное значение к текущему
            finalHitType = hitType;
        }
    }
    // Создаём объект типа попадания
    // Изначально с пустым значением
    BulletHit hit = null;

    // Если взрывной бонус отсутствует
    if (finalHitType == 0)
    {
        // Создаём обычное попадание
        hit = new DefaultBulletHit(finalHitType, _defaultHitPrefab);
    }
    // Иначе
    else
    {
        // Создаём попадание со взрывом
        hit = new ExplosionBulletHit(finalHitType, _explosionHitPrefab);
    }
    // Возвращаем объект типа попадания
    return hit;
}
// Применяем объект типа попадания к исходному объекту
private void Apply(BulletHit hit, GameObject root)
{
    // Ищем все компоненты в дочерних объектах root
    // Которые зависят от типа попадания
    IHitTypeBonusDependent[] dependents = root.GetComponentsInChildren<IHitTypeBonusDependent>();

    // Проходим по найденным компонентам
    for (int i = 0; i < dependents.Length; i++)
    {
        // Вызываем для каждого метод SetHit()
        dependents[i].SetHit(hit);
    }
}
}