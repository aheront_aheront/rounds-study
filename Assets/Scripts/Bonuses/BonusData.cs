﻿using System;
using UnityEngine;

[Serializable]
public class BonusData
{
    [SerializeField] private float _chance;
    [SerializeField] private BonusType _type;
// Группа бонусов
    [SerializeField] private int _group;

// Уровень бонусов
    [SerializeField] private int _level;

// Свойство для доступа к группе бонусов
    public int Group => _group;

// Свойство для доступа к уровню бонусов
    public int Level => _level;
    // НОВОЕ: Название (заголовок) бонуса
    [SerializeField] private string _title;

    public float Chance => _chance;
    public BonusType Type => _type;

    // НОВОЕ: Свойство для доступа к названию бонуса
    public string Title => _title;
}