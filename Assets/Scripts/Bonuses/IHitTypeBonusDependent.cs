﻿// Определяем интерфейс IHitTypeBonusDependent 
public interface IHitTypeBonusDependent 
{
    // Задаём пустой метод SetHit()
    void SetHit(BulletHit hit);
}