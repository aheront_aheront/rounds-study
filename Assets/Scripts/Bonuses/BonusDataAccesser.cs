﻿using System.Collections.Generic;
using UnityEngine;

public class BonusDataAccesser : MonoBehaviour
{
    // Массив данных о доступных бонусах
    [SerializeField] private BonusData[] _data;

    // Получаем список случайных бонусов
    public List<BonusData> GetRandomBonuses(List<BonusType> existingBonusTypes, int targetCount)
    {
        List<BonusData> possibleData = new List<BonusData>();
        List<BonusType> existingTypes = new List<BonusType>(existingBonusTypes);

        // НОВОЕ: Вызываем метод AddLowLevelBonuses()
        // Передаём в него список типов собранных бонусов
        AddLowLevelBonuses(existingTypes);

        for (int i = 0; i < targetCount; i++)
        {
            // НОВОЕ: Переписали условие проверки длины списка
            if (existingTypes.Count == _data.Length)
            {
                break;
            }
            BonusData randomBonus = GetRandomBonus(existingTypes);
            existingTypes.Add(randomBonus.Type);
            possibleData.Add(randomBonus);
        }
        return possibleData;
    }
    // Добавляем бонусы низшего уровня
    private void AddLowLevelBonuses(List<BonusType> bonuses)
    {
        // Объявляем переменную типа BonusData
        // Для хранения данных о бонусах
        BonusData data;

        // Проходим по всем бонусам в обратном порядке
        for (int i = bonuses.Count - 1; i >= 0; i--)
        {
            // Получаем данные о каждом бонусе по его типу
            data = GetBonusDataByType(bonuses[i]);

            // Проходим по массиву всех данных
            for (int j = 0; j < _data.Length; j++)
            {
                // Если этого типа бонуса нет в списке
                // И он принадлежит той же группе
                // И у него меньший уровень
                if (!bonuses.Contains(_data[j].Type) && _data[j].Group == data.Group && _data[j].Level < data.Level)
                {
                    // Добавляем этот тип бонуса в список
                    bonuses.Add(_data[j].Type);
                }
            }
        }
    }
// Получаем данные о бонусе по его типу
    private BonusData GetBonusDataByType(BonusType type)
    {
        // Проходим по всем бонусам
        for (int i = 0; i < _data.Length; i++)
        {
            // Если тип очередного бонуса совпадает с указанным
            if (_data[i].Type == type)
            {
                // Возвращаем данные этого бонуса
                return _data[i];
            }
        }
        // Возвращаем пустое значение
        return null;
    }
    // Получаем случайный бонус
    // Проверив, какие бонусы игрок уже собрал
    private BonusData GetRandomBonus(List<BonusType> existingBonusTypes)
    {
        // Получаем возможные бонусы для выбора
        List<BonusData> possibleData = GetPossibleData(existingBonusTypes);

        // Суммируем шансы всех возможных бонусов
        float sumChance = GetSumChance(possibleData);

        // Выбираем случайное число от 0 до суммы шансов
        float rand = Random.Range(0, sumChance);

        // Проходим по возможным бонусам
        for (int i = 0; i < possibleData.Count; i++)
        {
            // Если случайное число <= шансу бонуса
            if (rand <= possibleData[i].Chance)
            {
                // Возвращаем этот бонус
                return possibleData[i];
            }
            // Уменьшаем случайное число на шанс бонуса
            rand -= possibleData[i].Chance;
        }
        // Возвращаем пустое значение
        return null;
    }
    // Получаем список ещё не активированных бонусов
    private List<BonusData> GetPossibleData(List<BonusType> existingBonusTypes)
    {
        // Создаём новый список ещё не активированных бонусов
        List<BonusData> possibleData = new List<BonusData>();

        // Проходим по всему списку бонусов
        for (int i = 0; i < _data.Length; i++)
        {
            // Если в списке уже активированных бонусов
            // Ещё нет выбранного типа
            if (!existingBonusTypes.Contains(_data[i].Type))
            {
                // Добавляем этот бонус в список возможных
                possibleData.Add(_data[i]);
            }
        }
        // Возвращаем список возможных бонусов
        return possibleData;
    }
    // Вычисляем суммарный шанс получения бонусов из списка
    private float GetSumChance(List<BonusData> data)
    {
        // Начинаем с шанса 0
        float sumChance = 0;

        // Проходим по всему списку бонусов
        for (int i = 0; i < data.Count; i++)
        {
            // Добавляем шанс каждого бонуса к общему шансу
            sumChance += data[i].Chance;
        }
        // Возвращаем сумму шансов всех бонусов
        return sumChance;
    }
}