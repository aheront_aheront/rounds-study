﻿// Определяем интерфейс IShootCountBonusDependent 
public interface IShootCountBonusDependent 
{
    // Задаём пустой метод SetShootCount()
    void SetShootCount(int value);
}