using UnityEngine;

// Сделали класс абстрактным
public abstract class Bonus : MonoBehaviour
{
    // А здесь ничего не нужно
}
// Перечисление типов бонусов
public enum BonusType
{
    DoubleShoot,
    TripleShoot,
    QuadrupleShoot,

    // НОВОЕ: Слабый взрывной удар
    SmallExplosionHit,

    // НОВОЕ: Средний взрывной удар
    MediumExplosionHit,

    // НОВОЕ: Сильный взрывной удар
    LargeExplosionHit,
}